package org.example;

import java.util.*;

public class WithinTetrahedron {
    int a, b, c;
    ArrayList<ArrayList<Integer>> x = new ArrayList<>();
    static final int N = 3; // Matrix dimensions


    public WithinTetrahedron(String eval) {
        if (eval != null) parse(eval);
        else throw new IllegalArgumentException("There is empty Input!");
        if (volume(x) == 0) throw new IllegalArgumentException("Degenerate tetrahedron!");
    }

    private void parse(String eval) {
        int[] nums = eval.chars()
                .filter(Character::isDigit)
                .map(Character::getNumericValue)
                .toArray();
        a = nums[0];
        b = nums[1];
        c = nums[2];
        ArrayList<Integer> tmp = null;
        for (int i = 3; i < nums.length; i++) {
            if (i % 3 == 0) {
                if (tmp != null) x.add(tmp);
                tmp = new ArrayList<>(List.of(nums[i]));
            }
            else tmp.add(nums[i]);
        }
        x.add(tmp);
    }

    private static int determinantOfMatrix(ArrayList<ArrayList<Integer>> matrix)
    {
        int num1,
            num2,
            det = 1,
            index,
            total = 1;

        List<Integer> temp = new ArrayList<>(Collections.nCopies(N + 1, 0));

        for (int i = 0; i < N; i++) {
            index = i;
            while (index < N && matrix.get(index).get(i) == 0) index++;
            if (index == N) continue;
            if (index != i) {
                for (int j = 0; j < N; j++) swap(matrix, index, j, i, j);
                det = (int)(det * Math.pow(-1, index - i));
            }

            for (int j = 0; j < N; j++) temp.set(j, matrix.get(i).get(j));

            for (int j = i + 1; j < N; j++) {
                num1 = temp.get(i);
                num2 = matrix.get(j).get(i);

                for (int k = 0; k < N; k++) {
                    ArrayList<Integer> tmpArray = new ArrayList<>(matrix.get(j));
                    tmpArray.set(k, num1 * matrix.get(j).get(k) - num2 * temp.get(k));
                    matrix.set(j, tmpArray);
                }
                total = total * num1;
            }
        }

        for (int i = 0; i < N; i++) {
            det = det * matrix.get(i).get(i);
        }
        return (det / total);
    }

    private static void swap(ArrayList<ArrayList<Integer>> arr,
                             int i1, int j1, int i2, int j2)
    {
        int temp = arr.get(i1).get(j1);
        ArrayList<Integer> tmpArray = new ArrayList<>(arr.get(i1));
        tmpArray.set(j1, arr.get(i2).get(j2));
        arr.set(i1, tmpArray);
        tmpArray =  new ArrayList<>(arr.get(i2));
        tmpArray.set(j2, temp);
        arr.set(i2, tmpArray);
    }

    public static double volume(ArrayList<ArrayList<Integer>> x) {
        ArrayList<ArrayList<Integer>> v = new ArrayList<>();
        ArrayList<Integer> row;
        for (int i = 1; i < 4; i++) {
            row = new ArrayList<>(List.of(
                    x.get(i).get(0) - x.get(0).get(0),
                    x.get(i).get(1) - x.get(0).get(1),
                    x.get(i).get(2) - x.get(0).get(2)));
            v.add(row);
        }
        return ((double) Math.abs(determinantOfMatrix(v))) / 6;
    }

    public String calcWithinPoints() throws IllegalArgumentException {
        ArrayList<ArrayList<Integer>> s = new ArrayList<>();
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                for (int n = 0; n < c; n++) {
                    ArrayList<ArrayList<Integer>> d = new ArrayList<>(),
                            f = new ArrayList<>(),
                            h = new ArrayList<>(),
                            z = new ArrayList<>();
                    ArrayList<Integer> rowList = new ArrayList<>(List.of(i, j, n));
                    d.add(rowList);
                    f.add(rowList);
                    h.add(rowList);
                    z.add(rowList);

                    for (int k = 1; k < 4; k++) {
                        rowList = new ArrayList<>(List.of(x.get(k).get(0) - i, x.get(k).get(1) - j, x.get(k).get(2) - n));
                        d.add(rowList);
                    }
                    for (int k = 0; k < 3; k++) {
                        rowList = new ArrayList<>(List.of(x.get(k).get(0) - i, x.get(k).get(1) - j, x.get(k).get(2) - n));
                        f.add(rowList);
                    }
                    for (int k = 0; k < 4; k++) {
                        if (k == 2) continue;
                        rowList = new ArrayList<>(List.of(x.get(k).get(0) - i, x.get(k).get(1) - j, x.get(k).get(2) - n));
                        h.add(rowList);
                    }
                    for (int k = 0; k < 4; k++) {
                        if (k == 1) continue;
                        rowList = new ArrayList<>(List.of(x.get(k).get(0) - i, x.get(k).get(1) - j, x.get(k).get(2) - n));
                        z.add(rowList);
                    }
                    rowList = new ArrayList<>(List.of(i, j, n));
                    double volumeSum = (volume(d) + volume(f) + volume(h) + volume(z));
                    double xVol = volume(x);
//                    if (xVol - volumeSum <= Math.pow(10, -6)) s.add(rowList);
                    if (xVol == volumeSum && rowList.get(0) != 2) s.add(rowList);
                }
            }
        }
        return s.toString();
    }
}