# Is the point within tetrahedron?

### Description

Please, proceed to the [WithinTetrahedron](src/main/java/org/example/WithinTetrahedron.java) class 
whose constructor has 1 parameter, a string with data in the format `(a,b,c,[[i 1, j1, k1],[i2,j2,k2],[i3,j3,k3],[i4,j4,k4]])`.
Implement a method `calcWithinPoints` that returns a new string - a sequence of coordinates satisfying the following condition.

imagine a piece of space which is  `a` by `b` by `c` units apart from origin (a,b,c - are natural numbers)
The correct implementation should receive an array which consists of
4 arrays , where in each array will be triple of numbers which represent the coordinate of the vertex of tetrahedron .
Tetrahedron lies within the space which we imagined.Your method should return the array of integer coordinates in array form which lies within the tetrahedron.
If the tetrahedron is degenerated then you must throw IllegalArgumentException with next message: `Degenerated tetrahedron!`.
If there is `null` in input you must throw IllegalArgumentException with next message: `There is empty Input!`.

Details:
- Tetrahedron is convex
- Given array is guaranteed to be not null.
- You may use java.util.Arrays.* methods.

### Example

    Input :  (8,8,8,[[0,0,0],[0,0,6],[7,0,0],[0,7,0]])

    Output : [[0, 0, 0], [0, 0, 1], [0, 0, 2], [0, 0, 3], [0, 1, 0], [0, 1, 1], [0, 1, 2], [0, 2, 0], [0, 2, 1], [0, 3, 0], [1, 0, 0], [1, 0, 1], [1, 0, 2], [1, 1, 0], [1, 1, 1], [1, 2, 0], [2, 0, 0], [2, 0, 1], [2, 1, 0], [3, 0, 0]]

[//]: # ([[0, 0, 0], [0, 0, 3], [0, 0, 5], [0, 0, 6], [1, 0, 0], [1, 0, 3], [1, 0, 5], [2, 0, 3], [3, 0, 3], [4, 0, 3], [5, 0, 3], [6, 0, 3], [7, 0, 0], [7, 0, 2], [7, 0, 3]])
