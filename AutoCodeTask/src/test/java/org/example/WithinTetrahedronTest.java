package org.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class WithinTetrahedronTest {

    @Test
    void nullInput() {
        assertThrows(IllegalArgumentException.class,
                () -> new WithinTetrahedron(null),
                "There is empty Input!");
    }

    @ParameterizedTest
    @MethodSource
    void testDegenerateTetrahedron(String arg) {
        assertThrows(IllegalArgumentException.class,
                () -> new WithinTetrahedron(arg),
                "Degenerate tetrahedron!");
    }

    static Stream<String> testDegenerateTetrahedron() {
        return Stream.of(
                "(0,0,0[[0,0,0],[0,0,0],[0,0,0],[0,0,0]])",
                "(2,9,9,[[1,2,2],[3,2,2], [4,2,2], [1,2,2]])",
                "(1,1,1[[1,2,3],[3,2,1],[0,0,0],[0,0,0])",
                "(1,1,1[[1,1,1],[1,2,1],[0,3,0],[0,0,0])"
                );
    }

    @ParameterizedTest
    @MethodSource
    void calcWithinPoints(String input, String expected) {
        WithinTetrahedron withinTetrahedron = new WithinTetrahedron(input);
        String actual = withinTetrahedron.calcWithinPoints();
        assertEquals(expected, actual);
    }

    static Stream<Arguments> calcWithinPoints() {
        return Stream.of(
                arguments("(8,8,8,[[0,0,0],[0,0,6],[7,0,0],[0,7,0]])",
                        "[[0, 0, 0], [0, 0, 1], [0, 0, 2], [0, 0, 3], " +
                                "[0, 1, 0], [0, 1, 1], [0, 1, 2], [0, 2, 0], " +
                                "[0, 2, 1], [0, 3, 0], [1, 0, 0], [1, 0, 1], [1, 0, 2], " +
                                "[1, 1, 0], [1, 1, 1], [1, 2, 0], [3, 0, 0]]"),
                arguments("(5,9,9, [[0,2,2], [3,3,3], [4,4,4], [1,0,3]])",
                        "[[0, 1, 1]]"),
                arguments("(5,9,9,[[1,2,2],[3,3,3], [4,4,4], [1,2,3]])",
                        "[]")
        );
    }
}
